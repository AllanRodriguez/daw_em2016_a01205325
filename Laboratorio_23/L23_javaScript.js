/*jslint browser: true*/
/*global $, jQuery, alert, confirm, google*/

function initMap() {
    'use strict';
    
    var map, input, autocomplete, infowindow, marker, place, address;
    
    map = new google.maps.Map(document.getElementById('googleMap'), {
        center: {lat: 20.5887932, lng: -100.3898881},
        zoom: 13
    });
    input = /** @type {!HTMLInputElement} */(document.getElementById('pac-input'));
        
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    
    autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
    
    infowindow = new google.maps.InfoWindow();
    marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });
    
    autocomplete.addListener('place_changed', function () {
        infowindow.close();
        marker.setVisible(false);
        place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry LOL");
            return;
        }
        
        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setIcon(({ /** @type {google.maps.Icon} */
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);
        
        address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && (place.address_components[0].short_name || '')),
                (place.address_components[1] && (place.address_components[1].short_name || '')),
                (place.address_components[2] && (place.address_components[2].short_name || ''))
            ].join(' ');
        }
        
        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
    });
}