<?php
    require_once('L13_model.php');

    $function = $_GET['function'];
    $fr = $_GET['fr'];

    if ($function == "displayOne") {
        getFruitsByName($fr);
    } else if ($function == "displayAll") {
        getAllFruits();
    } else if ($function == "deleteOne") {
        if (eliminarFruta($fr)) {
            getAllFruits();
        } else {
            echo "<script type=\"text/javascript\">".
                "window.alert('¡No fue posible completar la petición!');".
                'window.location.href="index.php";'."</script>";
        }
    } else if ($function == "showHint") {
        $pattern = strtolower($_GET['hin']);
        $words = getNotDuplicatedFruits();
        $response = "";
        $size = 0;
        for ($i = 0; $i < count($words); $i++) {
            $pos = stripos(strtolower($words[$i]),$pattern);
            if(!($pos === false)) {
                $size++;
                $word = $words[$i];
                $response .= "<option value=\"$word\">$word</option>";
            }
        }
        if ($size > 0)
                echo "<select class=\"browser-default\" id=\"listFruits\" size=$size onclick=\"selectValue()\">$response</select>";
    }
?>
