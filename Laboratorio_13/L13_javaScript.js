/*jslint browser: true*/
/*jslint node: true */
/*global $, jQuery, alert*/

function getRequestObject() {
    "use strict";
    
    // Asynchronous objec created, handles browser DOM differences
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        return (new XMLHttpRequest());
    } else if (window.ActiveXObject) {
        // code for IE6, IE5
        return (new window.ActiveXObject("Microsoft.XMLHTTP"));
    } else {
        // Non AJAX browsers
        return (null);
    }
}

function displayOneFruit(fruta) {
    "use strict";
    
    var xmlhttp = getRequestObject();
    if (xmlhttp !== null) {
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                // Asynchronous response has arrived
                document.getElementById("resFruta").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", "L13_controller.php?function=displayOne&fr=" + fruta, true);
        xmlhttp.send();
    }
}

function displayAllFruit() {
    "use strict";
    
    var xmlhttp = getRequestObject();
    if (xmlhttp !== null) {
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                // Asynchronous response has arrived
                document.getElementById("resFruta").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", "L13_controller.php?function=displayAll&fr=None", true);
        xmlhttp.send();
    }
}

function deleteOneFruit(id) {
    "use strict";
        
    var xmlhttp = getRequestObject();
    if (xmlhttp !== null) {
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                // Asynchronous response has arrived
                document.getElementById("resFruta").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", "L13_controller.php?function=deleteOne&fr=" + id, true);
        xmlhttp.send();
    }
}

function showHint() {
    "use strict";
    
    var userInput, xmlhttp;
    
    xmlhttp = getRequestObject();
    if (xmlhttp !== null) {
        userInput = document.getElementById('fruta');
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                // Asynchronous response has arrived
                document.getElementById("hint").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", "L13_controller.php?function=showHint&fr=None&hin=" + userInput.value, true);
        xmlhttp.send();
    }
}

function selectValue() {
    "use strict";
    
    var list, userInput, ajaxResponse;
    
    list = document.getElementById("listFruits");
    userInput = document.getElementById("fruta");
    ajaxResponse = document.getElementById('hint');
    userInput.value = list.options[list.selectedIndex].text;
    ajaxResponse.style.visibility = "hidden";
    userInput.focus();
}

$(document).ready(function () {
    "use strict";
    $('#busqueda').css("padding-top", "+=60");
});

$(document).ready(function () {
    "use strict";
    $('#formBusqueda').submit(function (event) {
        var fruta = $("input[name=fruta]").val();
        if (fruta === "") {
            alert("Error: Campo Vacío");
        } else {
            document.getElementById("fruta").value = '';
            displayOneFruit(fruta);
        }
        event.preventDefault();
    });
});

$(document).ready(function () {
    "use strict";
    $('#replay').click(function () {
        displayAllFruit();
    });
});

require([
    "dojo/_base/fx",
    "dojo/on",
    "dojo/dom",
    "dojo/domReady!"],
        function (fx, on, dom) {
        "use strict";
        var fadeOutButton = dom.byId("fadeOutButton"),
            fadeInButton = dom.byId("fadeInButton"),
            fadeTarget = dom.byId("fadeTarget");

        on(fadeOutButton, "click", function (evt) {
            fx.fadeOut({ node: fadeTarget }).play();
        });
        on(fadeInButton, "click", function (evt) {
            fx.fadeIn({ node: fadeTarget }).play();
        });
    });