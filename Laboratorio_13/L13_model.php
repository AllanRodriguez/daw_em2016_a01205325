<?php 
    function conectDb() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "productos";
        
        $myDb = mysqli_connect($servername, $username, $password, $dbname);
        
        // Check Connection
        if (!$myDb) {
            die("Connection failed: " . mysqli_connect_error());
        } else
            return $myDb;
    }
    
    // La variable $myDb es una conexión establecida anteriormente
    function closeDb($myDb) {
        mysqli_close($myDb);
    }

    function getAllFruits() {
        $myDb = conectDb();
        
        // Specification of the SQL query
        $query = 'SELECT * FROM `frutas`';
        // Query execution; returns identifier of the result group
        $result = mysqli_query($myDb, $query);
        #$result = $myDb->query($query);
        
        if ($result == false) {
            echo mysql_errno($myDb) . ": " . mysql_error($myDb) . "\n";
        } else {
            echo Display($result);
        }
        
        // It releases the Associated Results
        mysqli_free_result($result);        
        closeDb($myDb);
    }

    function getNotDuplicatedFruits() {
        $myDb = conectDb();
        
        // Specification of the SQL query
        $query = 'SELECT DISTINCT `Nombre` FROM `frutas`';
        // Query execution; returns identifier of the result group
        $result = mysqli_query($myDb, $query);
        #$result = $myDb->query($query);
        
        if ($result == false) {
            echo mysql_errno($myDb) . ": " . mysql_error($myDb) . "\n";
        } else {
            $arr = array();
            $i = 0;
            while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                $arr[$i] = $row['Nombre'];
                $i++;
            }
            return $arr;
        }
        
        // It releases the Associated Results
        mysqli_free_result($result);        
        closeDb($myDb);
    }
    
    function getFruitsByName($nombre) {
        $myDb = conectDb();
        
        // Specification of the SQL query        
        $query = 'SELECT * FROM `frutas` WHERE `Nombre` LIKE "'.$nombre.'"';
        // Query execution; returns identifier of the result group
        $result = mysqli_query($myDb, $query);
        #$result = $myDb->query($query);
        
        if ($result == false) {
            echo mysql_errno($myDb) . ": " . mysql_error($myDb) . "\n";
        } else {
            echo Display($result);
        }
                       
        // It releases the Associated Results
        mysqli_free_result($result);        
        closeDb($myDb);
    }    

    function Display ($result) {
        if (($result->num_rows) == 0) {
            $table = "<strong>¡La Fruta Introducida NO Existe!</strong>";
            return $table;
        } else {
            $table = '<table class="striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Tipo</th>
                                <th>Cantidad</th>
                                <th>Precio (kg)</th>
                                <th>Pais</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>';

            // Cycle to Explode every Line of the Results
            while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                $table = $table . "<tr><td>" . $row['Id'] . "</td>";
                $table = $table . "<td>" . $row['Nombre'] . "</td>";
                $table = $table . "<td>" . $row['Tipo'] . "</td>";
                $table = $table . "<td>" . $row['Cantidad'] . "</td>";
                $table = $table . "<td>" . $row['Precio'] . "</td>";
                $table = $table . "<td>" . $row['Pais'] . "</td>";
                $table = $table . "<td><button class='btn waves-effect waves-light' type='button' onclick='deleteOneFruit(".$row['Id'].")'>
                <i class='material-icons'>delete</i></button>" . "</td></tr>";                
            }
            $table = $table . "</tbody></table>";

            return $table;
        }        
    }
    
    function insertarFruta($id, $nombre, $tipo, $cantidad, $precio, $pais) {
        $myDb = conectDb();
                
        $query = 'INSERT INTO `frutas` (id, nombre, tipo, cantidad, precio, pais) VALUES (?,?,?,?,?,?)';        
        // Preparing the statement
        if (!($statement = $myDb->prepare($query))) {
            return false;
            die("Preparation failed: (" . $myDb->errno . ") " . $myDb->error);
        }
        // Binding statement params
        if (!$statement->bind_param("ssssss", $id, $nombre, $tipo, $cantidad, $precio, $pais)) {
            return false;
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
        }
        // Executing the statement
        if (!$statement->execute()) {
            return false;
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
        }
        
        closeDb($myDb);
        return true;
    }

    function eliminarFruta($id) {
        $myDb = conectDb();
                
        // Deletion query construction
        $query= "DELETE FROM `frutas` WHERE `id`=?";
        if (!($statement = $myDb->prepare($query))) {
            die("The preparation failed: (" . $myDb->errno . ") " . $myDb->error);
        }
        // Binding statement params
        if (!$statement->bind_param("s", $id)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        } 
        // delete execution
        if ($statement->execute()) {
            //echo 'There were ' . mysqli_affected_rows($myDb) . ' affected rows';
        } else {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }
        
        closeDb($myDb);
        return true;
    }

    function modificarFruta($id, $nombre, $tipo, $cantidad, $precio, $pais) {
        $myDb = conectDb();
        
        $query = "UPDATE `frutas` SET nombre=?, tipo=?, cantidad=?, precio=?, pais=? WHERE `id`=?";        
        // Preparing the statement
        if (!($statement = $myDb->prepare($query))) {
            return false;
            die("Preparation failed: (" . $myDb->errno . ") " . $myDb->error);
        }
        // Binding statement params
        if (!$statement->bind_param("ssssss", $nombre, $tipo, $cantidad, $precio, $pais, $id)) {
            return false;
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
        }
        // Update execution
        if ($statement->execute()) {
            //echo 'There were ' . mysqli_affected_rows($mysql) . ' affected rows';
        } else {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }
        
        closeDb($myDb);
        return true;
    }
?>