/*jslint plusplus: true */

function validarPassword() {
	"use strict";
	var dom1, dom2;
	
	dom1 = document.valPassword.introPW.value;
	dom2 = document.valPassword.valPW.value;
	
	if ((dom1 === '') || (dom2 === '')) {
		window.alert("Uno o Ambos Campos están Vacíos");
	} else if (dom1 === dom2) {
		window.alert("El Password se Validó Correctamente");
    } else {
        window.alert("Validación Fallida. Intente de Nuevo");
    }
	
	document.getElementById("introPW").value = '';
	document.getElementById("valPW").value = '';
}

function mostrarOcultarPassword() {
	"use strict";
    var dom1, dom2, dom3;
	
    dom1 = document.getElementById("introPW");
    dom2 = document.getElementById("valPW");
	dom3 = document.getElementById("mosOcuPassword").checked;
	
	if (dom3 === true) {
		dom1.setAttribute("type", "password");
		dom2.setAttribute("type", "password");
	} else {
		dom1.setAttribute("type", "text");
		dom2.setAttribute("type", "text");
	}
}

var totPrecio, totCantidad, totIva, ptotal = 0, ctotal = 0, ivtotal = 0;

totPrecio = document.getElementById("totalPrecio");
totCantidad = document.getElementById("totalCantidad");
totIva = document.getElementById("totalIVA");


function costoProducto1() {
	"use strict";
    var dom1, cant1, tot1, iv1, de1, t1, promo;
	
	dom1 = document.getElementById("p1").checked;
	tot1 = document.getElementById("t1");
	iv1 = document.getElementById("iva1");
	de1 = document.getElementById("des1");
	promo = document.getElementById("promo3").checked;
	cant1 = document.getElementById("c1").value;
	cant1 = parseInt(cant1, 10);
	
	if (isNaN(cant1)) {
		tot1.innerHTML = '';
		iv1.innerHTML = '';
	} else {
		t1 = parseFloat((cant1 * 2.3 * 1.16), 10);
		iv1.innerHTML = "$ " + (t1 - (cant1 * 2.3)).toFixed(3);
		tot1.innerHTML = "$ " + t1.toFixed(3);
		
		if (dom1 === true) {
			if ((promo === true) && (cant1 < 100)) {
				t1 *= 2;
				de1.innerHTML = "$ -" + (t1 / 2).toFixed(3);
			}
			ivtotal += (t1 - (cant1 * 2.3));
			ptotal += t1;
			ctotal += cant1;
			totCantidad.innerHTML = ctotal + " Artículos";
			totPrecio.innerHTML = "$ " + ptotal.toFixed(3);
			totIva.innerHTML = "$ " + ivtotal.toFixed(3);
		}
	}
}

function costoProducto2() {
	"use strict";
	var dom2, cant2, tot2, iv2, de2, t2, promo;
	
	dom2 = document.getElementById("p2").checked;
	tot2 = document.getElementById("t2");
	iv2 = document.getElementById("iva2");
	de2 = document.getElementById("des2");
	promo = document.getElementById("promo2").checked;
	cant2 = document.getElementById("c2").value;
	cant2 = parseInt(cant2, 10);
	
	if (isNaN(cant2)) {
		tot2.innerHTML = '';
		iv2.innerHTML = '';
	} else {
		t2 = parseFloat((cant2 * 1.4 * 1.16), 10);
		iv2.innerHTML = "$ " + (t2 - (cant2 * 1.4)).toFixed(3);
		tot2.innerHTML = "$ " + t2.toFixed(3);
		
		if (dom2 === true) {
			if ((promo === true) && (cant2 >= 50) && (cant2 <= 100)) {
				de2.innerHTML = "$ " + (t2 - 100).toFixed(3);
				t2 = 100;
			}
			ivtotal += (t2 - (cant2 * 1.4));
			ptotal += t2;
			ctotal += cant2;
			totCantidad.innerHTML = ctotal + " Artículos";
			totPrecio.innerHTML = "$ " + ptotal.toFixed(3);
			totIva.innerHTML = "$ " + ivtotal.toFixed(3);
		}
	}
}
	
function costoProducto3() {
	"use strict";
	var dom3, cant3, tot3, iv3, de3, t3, promo;
	
	dom3 = document.getElementById("p3").checked;
	tot3 = document.getElementById("t3");
	iv3 = document.getElementById("iva3");
	de3 = document.getElementById("des3");
	promo = document.getElementById("promo1").checked;
	cant3 = document.getElementById("c3").value;
	cant3 = parseInt(cant3, 10);
		
	if (isNaN(cant3)) {
		tot3.innerHTML = '';
		iv3.innerHTML = '';
		de3.innerHTML = '';
	} else {
		t3 = parseFloat((cant3 * 3.1 * 1.16), 10);
		iv3.innerHTML = "$ " + (t3 - (cant3 * 3.1)).toFixed(3);
		tot3.innerHTML = "$ " + t3.toFixed(3);
		
		if (dom3 === true) {
			if ((promo === true) && (cant3 === 1000)) {
				t3 -= 3.1;
				de3.innerHTML = "$ " + 3.100;
			}
			ivtotal += (t3 - (cant3 * 3.1));
			ptotal += t3;
			ctotal += cant3;
			totCantidad.innerHTML = ctotal + " Artículos";
			totPrecio.innerHTML = "$ " + ptotal.toFixed(3);
			totIva.innerHTML = "$ " + ivtotal.toFixed(3);
		}
	}
}

document.getElementById("c1").oninput = costoProducto1;
document.getElementById("c2").oninput = costoProducto2;
document.getElementById("c3").oninput = costoProducto3;

function borrarContenido() {
	"use strict";
	document.getElementById("producto1").reset();
	document.getElementById("cantidad1").reset();
	document.getElementById("producto2").reset();
	document.getElementById("cantidad2").reset();
	document.getElementById("producto3").reset();
	document.getElementById("cantidad3").reset();
	totCantidad.innerHTML = '';
	totPrecio.innerHTML = '';
	totIva.innerHTML = '';
	document.getElementById("t1").innerHTML = '';
	document.getElementById("iva1").innerHTML = '';
	document.getElementById("des1").innerHTML = '';
	document.getElementById("t2").innerHTML = '';
	document.getElementById("iva2").innerHTML = '';
	document.getElementById("des2").innerHTML = '';
	document.getElementById("t3").innerHTML = '';
	document.getElementById("iva3").innerHTML = '';
	document.getElementById("des3").innerHTML = '';
	ptotal = 0;
	ctotal = 0;
	ivtotal = 0;
}

document.getElementById("fecha").addEventListener("click", function () {
	"use strict";
	document.getElementById("fecha").innerHTML = Date();
});

function calculadora() {
	"use strict";
	var num1, num2, op, res;
	
	num1 = document.getElementById("nUno").value;
	num1 = parseInt(num1, 10);
	num2 = document.getElementById("nDos").value;
	num2 = parseInt(num2, 10);
	op = document.getElementById("op").value;
	res = document.getElementById("resultado");
	
	if (isNaN(num1) || isNaN(num2)) {
		res.innerHTML = "Error: Introduce solo Números";
	} else {
		if (op === "+") {
			res.innerHTML = num1 + num2;
		} else if (op === "-") {
			res.innerHTML = num1 - num2;
		} else if (op === "*") {
			res.innerHTML = num1 * num2;
		} else if (op === "/") {
			res.innerHTML = (num1 / num2).toFixed(5);
		} else if (op === "^") {
			res.innerHTML = Math.pow(num1, num2);
		} else {
			res.innerHTML = "Error: Operador Inválido";
		}
	}
}

function borrarContenidoCalculadora() {
	"use strict";
	document.getElementById("numeroUno").reset();
	document.getElementById("operador").reset();
	document.getElementById("numeroDos").reset();
	document.getElementById("resultado").innerHTML = '';
}