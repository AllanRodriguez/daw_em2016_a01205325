<?php 
    function conectDb() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "productos";
        
        $myDb = mysqli_connect($servername, $username, $password, $dbname);
        
        // Check Connection
        if (!$myDb) {
            die("Connection failed: " . mysqli_connect_error());
        } else
            return $myDb;
    }
    
    // La variable $myDb es una conexión establecida anteriormente
    function closeDb($myDb) {
        mysqli_close($myDb);
    }

    function getFruits() {
        $myDb = conectDb();
        
        // Specification of the SQL query
        $query = 'SELECT * FROM `frutas`';
        // Query execution; returns identifier of the result group
        $result = mysqli_query($myDb, $query);
        #$result = $myDb->query($query);
        
        echo Display($result);
        
        // It releases the Associated Results
        mysqli_free_result($result);        
        closeDb($myDb);
    }

    // Consulta Uno
    function getFruitsByName($nombre) {
        $myDb = conectDb();
        
        // Specification of the SQL query        
        $query = 'SELECT * FROM `frutas` WHERE `Nombre` LIKE "'.$nombre.'"';
        // Query execution; returns identifier of the result group
        $result = mysqli_query($myDb, $query);
        #$result = $myDb->query($query);
        
        Display($result);
        
        // It releases the Associated Results
        mysqli_free_result($result);        
        closeDb($myDb);
    }

    // Consulta Dos
    function getFruitsByCountries($country1, $country2) {
        $myDb = conectDb();
        
        // Specification of the SQL query
        $query = 'SELECT * FROM `frutas` WHERE `Pais` LIKE "'.$country1.'" OR `Pais` LIKE "'.$country2.'"';
        // Query execution; returns identifier of the result group
        $result = mysqli_query($myDb, $query);
        #$result = $myDb->query($query);
        
        echo Display($result);
        
        // It releases the Associated Results
        mysqli_free_result($result);        
        closeDb($myDb);
    }

    // Consulta Tres
    function getFruitsByPrice($minPrice, $maxPrice) {
        $myDb = conectDb();
        
        // Specification of the SQL query
        $query = 'SELECT * FROM `frutas` WHERE `Precio` >= "'.$minPrice.'" AND `Precio` <= "'.$maxPrice.'"';
        // Query execution; returns identifier of the result group
        $result = mysqli_query($myDb, $query);
        #$result = $myDb->query($query);
        
        echo Display($result);
        
        // It releases the Associated Results
        mysqli_free_result($result);        
        closeDb($myDb);
    }

    function Display ($result) {        
        $table = '<table class="striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Cantidad</th>
                            <th>Precio (kg)</th>
                            <th>Pais</th>
                        </tr>
                    </thead>
                    <tbody>';
        
        // Cycle to Explode every Line of the Results
        while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
            $table = $table . "<tr><td>" . $row['Id'] . "</td>";
            $table = $table . "<td>" . $row['Nombre'] . "</td>";
            $table = $table . "<td>" . $row['Tipo'] . "</td>";
            $table = $table . "<td>" . $row['Cantidad'] . "</td>";
            $table = $table . "<td>" . $row['Precio'] . "</td>";
            $table = $table . "<td>" . $row['Pais'] . "</td></tr>";
        }
        $table = $table . "</tbody></table>";
        
        return $table;
    }
    
    function insertarFruta($id, $nombre, $tipo, $cantidad, $precio, $pais) {
        $myDb = conectDb();
        
        // insert command specification
        /*$query = "INSERT INTO frutas (nombre, tipo, cantidad, precio, pais) VALUES (\"" . $nombre . "\",\"" . $tipo .
            "\"," . $cantidad . "," . $precio . ",\"" . $pais . "\");";
        if (mysqli_query($myDb, $query)) {
            echo "New record created successfully";
            closeDb($myDb);
            return true;
        } else {
            echo "Error: " . $query . "<br>" . mysqli_error($myDb);
            closeDb($myDb);
            return false;
        }*/
        $query = 'INSERT INTO `frutas` (id, nombre, tipo, cantidad, precio, pais) VALUES (?,?,?,?,?,?)';        
        // Preparing the statement
        if (!($statement = $myDb->prepare($query))) {
            return false;
            die("Preparation failed: (" . $myDb->errno . ") " . $myDb->error);
        }
        // Binding statement params
        if (!$statement->bind_param("ssssss", $id, $nombre, $tipo, $cantidad, $precio, $pais)) {
            return false;
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
        }
        // Executing the statement
        if (!$statement->execute()) {
            return false;
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
        }
        
        closeDb($myDb);
        return true;
    }

    function eliminarFruta($id) {
        $myDb = conectDb();
                
        // Deletion query construction
        $query= "DELETE FROM `frutas` WHERE `id`=?";
        if (!($statement = $myDb->prepare($query))) {
            die("The preparation failed: (" . $myDb->errno . ") " . $myDb->error);
        }
        // Binding statement params
        if (!$statement->bind_param("s", $id)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        } 
        // delete execution
        if ($statement->execute()) {
            echo 'There were ' . mysqli_affected_rows($myDb) . ' affected rows';
        } else {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }
        
        closeDb($myDb);
        return true;
    }

    function modificarFruta($id, $nombre, $tipo, $cantidad, $precio, $pais) {
        $myDb = conectDb();
        
        $query = "UPDATE `frutas` SET nombre=?, tipo=?, cantidad=?, precio=?, pais=? WHERE `id`=?";        
        // Preparing the statement
        if (!($statement = $myDb->prepare($query))) {
            return false;
            die("Preparation failed: (" . $myDb->errno . ") " . $myDb->error);
        }
        // Binding statement params
        if (!$statement->bind_param("ssssss", $nombre, $tipo, $cantidad, $precio, $pais, $id)) {
            return false;
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
        }
        // Update execution
        if ($statement->execute()) {
            echo 'There were ' . mysqli_affected_rows($mysql) . ' affected rows';
        } else {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }
        
        closeDb($myDb);
        return true;
    }
?>