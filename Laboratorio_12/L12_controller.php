<?php
    require_once('L12_util.php');
    session_start();

    if (isset($_SESSION["function"])) {
        if ($_SESSION["function"] == 'insert') {
            insert_fruit();
        }
        else if ($_SESSION["function"] == 'delete') {
            delete_fruit();
        }
        else if ($_SESSION["function"] == 'update') {
            update_fruit();
        }
    }
    
    if (isset($_POST["function"])) {
        $_SESSION["function"] = $_POST["function"];
        if (($_SESSION["function"] == 'insert') | ($_SESSION["function"] == 'update')) {
            include('_formFormulario1.html');
        } else if ($_SESSION["function"] == 'delete') {
            include('_formFormulario2.html');
        }
    } else {
        echo "<script type=\"text/javascript\">".
            "window.alert('¡Opción No Seleccionada!');".
            'window.location.href="index.php";'."</script>";
    }

    function insert_fruit() {
        $id = $_POST["id"];
        $nombre = $_POST["nombre"];
        $tipo = $_POST["tipo"];
        $cantidad = $_POST["cantidad"];
        $precio = $_POST["precio"];
        $pais = $_POST["pais"];
        
        if (insertarFruta($id, $nombre, $tipo, $cantidad, $precio, $pais)) {
            header("location: index.php");
        } else {
            echo "<script type=\"text/javascript\">".
                "window.alert('¡No fue posible completar la petición!');".
                'window.location.href="index.php";'."</script>";
        }
    }

    function delete_fruit() {
        $id = $_POST["id"];
        
        if (eliminarFruta($id)) {
            header("location: index.php");
        } else {
            echo "<script type=\"text/javascript\">".
                "window.alert('¡No fue posible completar la petición!');".
                'window.location.href="index.php";'."</script>";
        }
    }
    
    function update_fruit() {
        $id = $_POST["id"];
        $nombre = $_POST["nombre"];
        $tipo = $_POST["tipo"];
        $cantidad = $_POST["cantidad"];
        $precio = $_POST["precio"];
        $pais = $_POST["pais"];
        
        if (modificarFruta($id, $nombre, $tipo, $cantidad, $precio, $pais)) {
            header("location: index.php");
        } else {
            echo "<script type=\"text/javascript\">".
                "window.alert('¡No fue posible completar la petición!');".
                'window.location.href="index.php";'."</script>";
        }
    }
?>
