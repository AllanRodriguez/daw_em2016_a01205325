<!--
    Laboratorio 8: Introducción a PHP
    Autor: Allan Uriel Rodríguez Godínez
    Editor: Brackets
-->

<!DOCTYPE html>
<html>
    <head>
        <meta content="text/html; charset=UTF-8" http-equiv="content-type">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Laboratorio 8: Introducción a PHP</title>
                    		 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
		        
        <?php include_once("myLibPHP.php");?>
    </head>
    <body>
        <header>
            <nav class="top-nav">
                <div class="container">
                    <div class="nav-wrapper">
                        <a class="brand-logo center">Laboratorio 8: Introducción a PHP</a>
                    </div>
                </div>
            </nav>            
        </header>
        <main>
            <div class="container">
                <div class="row">
                    <div class="col l2">
                        <ul id="nav_mobile" class="side-nav fixed" style="width:250px;">
                            <li class="logo center-align"><i id="alignLogo" class="large material-icons">done_all</i></li>
                            <li class="light italic"><a id="alignSideNav" href="#POne" class="waves-effect waves-teal center-align">Problema 1</a></li>
                            <li class="light italic"><a href="#PTwo" class=" waves-effect waves-teal center-align">Problema 2</a></li>
                            <li class="light italic"><a href="#PThree" class="waves-effect waves-teal center-align">Problema 3</a></li>
                            <li class="light italic"><a href="#PFour" class="waves-effect waves-teal center-align">Problema 4</a></li>
                            <li class="light italic"><a href="#PFive" class="waves-effect waves-teal center-align">Problema 5</a></li>
                            <li class="light italic"><a href="#Questions" class="waves-effect waves-teal center-align">Preguntas Teóricas</a></li>
                        </ul>
                    </div>
                    <div class="col l10">
                        <div class="section scrollspy">
                            <div class="section" id="POne">
                                <h4>Problema 1</h4>
                                <blockquote>Función que recibe un arreglo de números y devuelve su promedio.</blockquote>
                                <p>El Arreglo de Números es:  8, 5, 6, 10, 2, 7, 9, 15, 0, 3</p>                                
                                <?php
                                    $arr =  array(8, 5, 6, 10, 2, 7, 9, 15, 0, 3);
                                    $num = promedio($arr);
                                    echo "El Promedio del Arreglo es: $num";
                                ?>
                            </div>
                            <div class="divider"></div>
                            <div class="section" id="PTwo">
                                <h4>Problema 2</h4>
                                <blockquote>Función que recibe un arreglo de números y devuelve su mediana.</blockquote>
                                <p>El Arreglo de Números es:  13, 7, 5, 21, 23, 23, 40, 23, 14, 12, 23, 29</p>
                                <?php
                                    $arr =  array(13, 7, 5, 21, 23, 23, 40, 23, 14, 12, 23, 29);                                    
                                    $num = mediana($arr);
                                    echo "La Mediana del Arreglo es: $num";  
                                ?>
                            </div>
                            <div class="divider"></div>
                            <div class="section" id="PThree">
                                <h4>Problema 3</h4>
                                <blockquote>Función que recibe un arreglo de números y muestra la lista de números, y como ítems de una lista HTML muestra el promedio, la media, y el arreglo ordenado de menor a mayor, y posteriormente de mayor a menor.</blockquote>
                                <p>El Arreglo de Números es: 5, 8, 12, 0, 3, 8, 1, 6, 17, 9</p>
                                <ul class="collection">
                                    <li class="collection-item">
                                        <?php
                                            $arr =  array(5, 8, 12, 0, 3, 8, 1, 6, 17, 9);
                                            $num = promedio($arr);
                                            echo "El Promedio del Arreglo es: $num";
                                        ?>
                                    </li>
                                    <li class="collection-item">
                                        <?php
                                            $arr =  array(5, 8, 12, 0, 3, 8, 1, 6, 17, 9);
                                            $num = promedio($arr);
                                            echo "La Media del Arreglo es: $num";
                                        ?>
                                    </li>
                                    <li class="collection-item">
                                        <?php
                                            echo "El Arreglo Ordenado de Menor a Mayor: ";
                                            $arr =  array(5, 8, 12, 0, 3, 8, 1, 6, 17, 9);
                                            $num = orderMenMay($arr);
                                            printArray($num);
                                        ?>
                                    </li>
                                    <li class="collection-item">
                                        <?php
                                            echo "El Arreglo Ordenado de Mayor a Menor: ";
                                            $arr =  array(5, 8, 12, 0, 3, 8, 1, 6, 17, 9);
                                            $num = orderMayMen($arr);
                                            printArray($num);
                                        ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="divider"></div>
                            <div class="section" id="PFour">
                                <h4>Problema 4</h4>
                                <blockquote>Función que imprime una tabla HTML, que muestra los cuadrados y cubos de 1 hasta un número 'n'.</blockquote>
                                <p>Número(n) = 5</p>
                                <table class="striped centered">
                                    <tbody>
                                        <thead>
                                            <tr>
                                                <th>Número</th>
                                                <th>Cuadrado</th>
                                                <th>Cubo</th>
                                            </tr>
                                        </thead>
                                        <?php $n=5; for ($i=1;$i<=$n;$i++): ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $i*$i; ?></td>
                                            <td><?php echo $i*$i*$i; ?></td>
                                        </tr>
                                        <?php endfor; ?>
                                    </tbody>
                                </table>                                
                            </div>
                            <div class="divider"></div>
                            <div class="section" id="PFive">
                                <h4>Problema 5</h4>
                                <blockquote>Escoge algún problema que hayas implementado en otro lenguaje de programación, y dale una solución en php, en una vista agradable para el usuario.</blockquote>
                                <p>Descripción: Se desea calcular el valor aproximado del número Pi a través de un método de integración numérica.</p>
                                <img class="responsive-img" src="../imagenpi.jpg">
                                <div class="row">
                                    <div class="col l9">
                                        <div class="card-panel grey lighten-2">
                                            <span class="black-text">"The numerical integration midpoint rectangle rule to solve the integral just shown. To compute an approximation of the area under the curve, we must compute the area of some number of rectangles (num_rects) by finding the midpoint (mid) of each rectangle and computing the height of that rectangle (height), which is simply the function value at that midpoint. We add together the heights of all the rectangles (sum) and, once computed, we multiply the sum of the heights by the width of the rectangles (width) to determine the desired approximation of the total area (area) and the value of pi"(BRESHEARS, pp. 31 y 32) [1].
                                            </span>
                                        </div>
                                    </div>
                                </div>
				                <p>Nota: A mayor cantidad de rectángulos la aproximación del número Pi será mejor. Número de Rectángulos = 1 000 000.</p>
                                <?php                                    
                                    $numRects = 1000000;
                                    $num = CalculoPi($numRects);                                                                    
                                    echo "El Valor de Pi es: $num";
                                    #phpinfo();
                                ?>
                            </div>
                        </div>
                        <div class="section scrollspy">
                            <div class="section" id="Questions">
                                <h4>Preguntas Teóricas</h4>
                                <dl>
                                    <dt>1. ¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.</dt>
                                    <dd>Esta función muestra gran cantidad de información sobre el estado actual de PHP. Incluye información sobre las opciones de compilación y extensiones de PHP, versión de PHP, información del servidor y entorno (si se compiló como módulo), entorno PHP, versión del OS, rutas, valor de las opciones de configuración locales y generales, cabeceras HTTP y licencia de PHP.<br>Como cada sistema se instala diferente, phpinfo() se usa comúnmente para revisar opciones de configuración y variables predefinidas disponibles en un sistema dado. También es una valiosa herramienta de depuración ya que contiene todos valores EGPCS (Environment, GET, POST, Cookie, Server).<br>Los datos que llaman mi atención son los referentes a la información del servidor y del entorno, ya que estos son datos de gran relevancia que en manos equivocadas podrían perjudicar al servidor, también es interesante conocer todos los desarrolladores detrás de este gran proyecto, listados al final de los resultados.</dd>
                                    <dt>2. ¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</dt>
                                    <dd>Las distribuciones de Apache, e.g. XAMPP, no están destinadas a entornos de producción, sino a entornos de desarrollo, por lo que la configuración por defecto no es lo suficiente segura para un ambiente de producción.<br>Para mejorar la seguridad es necesario establecer contraseñas para el usuario ROOT de la base de datos, con lo cual se limita el acceso a conexiones no deseadas.</dd>
                                    <dt>3. ¿Cómo es que si el código está en un archivo con código HTML que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.</dt>
                                    <dd>PHP es un lenguaje de scripts del lado del servidor. Los scripts de PHP están incrustados en los documentos HTML y el servidor los interpreta y ejecuta antes de servir las páginas al cliente, por lo cual, el cliente no ve el código PHP, sólo los resultados que la ejecución produce.</dd>
                                </dl>
                            </div>
                            <div class="divider"></div>
                            <div class="section" id="References">
                                <h5>Referencias</h5>
                                <ul>
                                    <li>[1] Herrera Ríos, E. (2011, Octubre). Arrancar con HTML. Primera Edición. México: Alfaomega Grupo Editor, S.A. de C.V.</li>                                     
                                    <li>[2] Materialize (s.f.). Material Design. Recuperado de <a href="http://materializecss.com/about.html">MaterializeCSS.com</a></li>
                                    <li>[3] PHP (s.f.). PHP Manual. Recuperado de <a href="http://php.net/manual/en/">PHP.net</a></li>
                                </ul>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <footer class="page-footer">
            <div class="container">
                <div class="row">
                    <div class="col l5 push-l9"><span class="white-text flow-text">Allan Uriel Rodríguez Godínez | A01205325</span></div>                 
                </div>
            </div>
        </footer>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
    </body>
</html>