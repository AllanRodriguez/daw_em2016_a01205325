<?php
    function promedio($arr) {
        $num = 0;
        for ($i=0; $i<count($arr); $i++) {
            $num += $arr[$i];
        }
        return $num / count($arr);
    }

    function mediana($arr) {
        $num = 0;
        sort($arr);
        if (count($arr) % 2 != 0)
            $num = $arr[floor(count($arr)/2)];        
        else if (count($arr) % 2 == 0)
            $num = ($arr[count($arr)/2] + $arr[(count($arr)/2) - 1]) / 2;    
        return $num;
    }

    function orderMenMay($arr) {
        sort($arr);  
        return $arr;
    }

    function orderMayMen($arr) {
        rsort($arr);  
        return $arr;
    }

    function printArray($arr) {
        for ($i=0; $i<count($arr); $i++) {
            if ($i < count($arr) - 1)
                echo "$arr[$i], ";
            else
                echo "$arr[$i]";
        }
    }

    function CalculoPi($numRects) {
        $sum = 0;
        $width = 1 / $numRects;
        
        for ($i=0; $i<$numRects; $i++) {
            $mid = ($i + 0.5) * $width;
            $height = 4 / (1 + $mid * $mid);
            $sum += $height;
        }
        $area = $width * $sum;
        return $area;
    }
?>
