<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

$app->get('/hello/{name}', function ($name) {
    return "Hola ".$name."!";
});

$app->get('/', function () {
    return 'The service is up and running!';
});

$app->get('/zeller/{dia}/{mes}/{anio}', function ($dia ,$mes ,$anio) {
    if (($mes == "Febrero") && ($dia > 28)) {
        return "¡FEBRERO solo tiene 28 días LOL!";
    } else if (($mes == "Abril") && ($dia > 30)) {
        return "¡ABRIL solo tiene 30 días LOL!";
    } else if (($mes == "Junio") && ($dia > 30)) {
        return "¡JUNIO solo tiene 30 días LOL!";
    } else if (($mes == "Septiembre") && ($dia > 30)) {
        return "¡SEPTIEMBRE solo tiene 30 días LOL!";
    } else if (($mes == "Noviembre") && ($dia > 30)) {
        return "¡NOVIEMBRE solo tiene 30 días LOL!";
    } else {
        switch ($mes) {
                case "Enero":
                    $month = 1;
                break;
                case "Febrero":
                    $month = 2;
                break;
                case "Marzo":
                    $month = 3;
                break;
                case "Abril":
                    $month = 4;
                break;
                case "Mayo":
                    $month = 5;
                break;
                case "Junio":
                    $month = 6;
                break;
                case "Julio":
                    $month = 7;
                break;
                case "Agosto":
                    $month = 8;
                break;
                case "Septiembre":
                    $month = 9;
                break;
                case "Octubre":
                    $month = 10;
                break;
                case "Noviembre":
                    $month = 11;
                break;
                    case "Diciembre":
                    $month = 12;
                break;
                default:
                    $month = 1;
        }
    
        $a = floor((14 - $month) / 12);
        $y = $anio - $a;
        $m = $month + (12 * $a) - 2;
        $d = ($dia + $y + floor($y/4) - floor($y/100) + floor($y/400) + floor((31*$m)/12)) % 7;        

        switch ($d) {
            case 0:
                $day = "Domingo";
                break;
            case 1:
                $day = "Lunes";
                break;
            case 2:
                $day = "Martes";
                break;
            case 3:
                $day = "Miércoles";
                break;
            case 4:
                $day = "Jueves";
                break;
            case 5:
                $day = "Viernes";
                break;
            case 6:
                $day = "Sábado";
                break;
            default:
                $day = "Domingo";
        }
        return $day;
    }
});

$app->run();
