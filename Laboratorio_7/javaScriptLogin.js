/*jslint browser: true*/
/*global $, jQuery, alert*/

$('.toggle').on('click', function () {
	'use strict';
	$('.container').stop().addClass('active');
});

$('.close').on('click', function () {
	'use strict';
	$('.container').stop().removeClass('active');
});