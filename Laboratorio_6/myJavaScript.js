/*jslint plusplus: true */

function changeTextStyle() {
	"use strict";
	document.getElementById("textTwo").style.fontStyle = "italic";
	document.getElementById("textTwo").style.fontWeight = "bold";
}

function returnTextStyle() {
	"use strict";
	document.getElementById("textTwo").style.fontStyle = "normal";
	document.getElementById("textTwo").style.fontWeight = "normal";
}

function changeTextColorDown() {
	"use strict";
    document.getElementById("textTwo").style.color = "red";
}

function changeTextColorUp() {
	"use strict";
    document.getElementById("textTwo").style.color = "black";
}

function changeTextFontOver() {
	"use strict";
    document.getElementById("textTwo").style.fontFamily = "Lucida Console";
}

function changeTextFontOut() {
	"use strict";
    document.getElementById("textTwo").style.fontFamily = "Times New Roman";
}

function changeTextSize() {
	"use strict";
	var x;
	
	x = document.getElementById("eventOne").value;
	
	if (x.length === 1) {
		document.getElementById("textTwo").style.fontSize = "small";
	} else {
		document.getElementById("textTwo").style.fontSize = "17px";
		document.getElementById("eventOne").value = '';
	}
}

function calculadora() {
	"use strict";
	var num1, num2, op, res;
	
	num1 = document.getElementById("nUno").value;
	num2 = document.getElementById("nDos").value;
	op = document.getElementById("op").value;
	res = document.getElementById("resultado");
	
	if (isNaN(num1) || isNaN(num2)) {
		res.innerHTML = "ERROR";
		window.alert("Error: Introduce solo Números");
	} else {
		if (op === "+") {
			res.innerHTML = (Number(num1) + Number(num2)).toLocaleString();
		} else if (op === "-") {
			res.innerHTML = (Number(num1) - Number(num2)).toLocaleString();
		} else if (op === "*") {
			res.innerHTML = (Number(num1) * Number(num2)).toLocaleString();
		} else if (op === "/") {
			res.innerHTML = (Number(num1) / Number(num2)).toLocaleString();
		} else if (op === "%") {
			res.innerHTML = (Number(num1) % Number(num2)).toLocaleString();
		} else if (op === "^") {
			res.innerHTML = (Math.pow(Number(num1), Number(num2))).toLocaleString();
		} else if (op === "&") {
			res.innerHTML = (Number(num1 + num2)).toLocaleString();
		} else {
			res.innerHTML = "ERROR";
			window.alert("Error: Operador Inválido");
		}
	}
}

function borrarContenidoCalculadora() {
	"use strict";
	document.getElementById("numeroUno").reset();
	document.getElementById("operador").reset();
	document.getElementById("numeroDos").reset();
	document.getElementById("resultado").innerHTML = "";
}

function displayInfo(num) {
	"use strict";
	if (num === 1) {
		document.getElementById("informationOne").style.display = "block";
	} else if (num === 2) {
		document.getElementById("informationThree").style.display = "block";
	} else if (num === 3) {
		document.getElementById("informationTwo").style.display = "block";
	} else {
		document.getElementById("informationFour").style.display = "block";
	}
}

function hideInfo(num) {
	"use strict";
	if (num === 1) {
		document.getElementById("informationOne").style.display = "none";
	} else if (num === 2) {
		document.getElementById("informationThree").style.display = "none";
	} else if (num === 3) {
		document.getElementById("informationTwo").style.display = "none";
	} else {
		document.getElementById("informationFour").style.display = "none";
	}
}

function allowDrop(ev) {
	"use strict";
    ev.preventDefault();
}

function drag(ev) {
	"use strict";
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
	"use strict";
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}

function moverBarra() {
	"use strict";
	var elem, width, id;
  
	elem = document.getElementById("Barra");
	width = 0;
	
	function frame() {
		if (width === 100) {
			clearInterval(id);
		} else {
			width++;
			elem.style.width = width + '%';
		}
	}
	id = setInterval(frame, 100);
}

function borrarBarra() {
	"use strict";
	var elem, width, id;
  
	elem = document.getElementById("Barra");
	width = 100;
	
	function frame() {
		if (width > 0) {
			width--;
			elem.style.width = width + '%';
		} else {
			clearInterval(id);
		}
	}
	id = setInterval(frame, 100);
}

var time;

function myTimer() {
	"use strict";
    var d, t;
	
	d = new Date();
    t = d.toLocaleTimeString();
    document.getElementById("hora").innerHTML = "Reloj Digital: " + t;
}

time = setInterval(function () {"use strict"; myTimer(); }, 1000);