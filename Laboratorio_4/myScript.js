// Problema 1
/*jslint plusplus: true */

function tablaCuadCubo() {
    "use strict";
	var i, j, text, num, bodyHTML, tTable, tBody, tRow, tCell, ttCell, tHeader;

	num = window.prompt("Introducir un Número:");
    num = parseInt(num, 10);
	
	bodyHTML = document.getElementsByTagName("form")[0];
    tTable = document.createElement("table");
    tBody = document.createElement("tbody");
	tHeader = ["-", "Número", "Cuadrado", "Cubo"];
	
    if (isNaN(num) || (num === 0)) {
        text = "ERROR: Introducir solo Números";
    } else {
		text = "NOTA: Recibido Correctamente";

		for (i = 0; i <= num; i++) {
			tRow = document.createElement("tr");
			for (j = 1; j <= 3; j++) {
				tCell = document.createElement("td");
				if (i === 0) {
					ttCell = document.createTextNode(tHeader[j]);
				} else {
					ttCell = document.createTextNode(Math.pow(i, j));
				}
				tCell.appendChild(ttCell);
				tRow.appendChild(tCell);
			}
			tBody.appendChild(tRow);
		}
		tTable.appendChild(tBody);
		bodyHTML.appendChild(tTable);
		tTable.setAttribute("border");
    }
    document.getElementById("salidaP1").innerHTML = text;
}

// Problema 2

function sumNumAleatorios() {
	"use strict";
	var tInicio, tFin, num1, num2, sum, val;
	    
    num1 = Math.floor((Math.random() * 1000) + 1);
    num2 = Math.floor((Math.random() * 1000) + 1);
    sum = num1 + num2;
	
	tInicio = window.performance.now();
	val = window.prompt("Indicar el Resultado de: " + num1 + " + " + num2);
    val = parseInt(val, 10);
        
    tFin = (window.performance.now() - tInicio) / 1000;
    tFin = tFin.toFixed(4);
    if (val === sum) {
		window.alert("¡Correcto!\nTiempo de Respuesta: " + tFin + " segundos");
    } else {
        window.alert("¡Incorrecto!\nRespuesta Correcta: " + sum + "\nTiempo de Respuesta: " + tFin + " segundos");
    }
}

// Problema 3

function contador() {
	"use strict";
	var i, text, arr, tam, pos, neg, ceros, x, num = " ";
	
	num = document.getElementById("entrada3").value;
    arr = num.split(",");
    tam = arr.length - 1;
    pos = 0;
    neg = 0;
    ceros = 0;
	
    for (i = 0; i <= tam; i++) {
		x = parseInt(arr[i], 10);
        
        if (x === 0) {
			ceros++;
        } else if (x < 0) {
			neg++;
        } else {
            pos++;
        }
    }
    text = "Números Positivos: " + pos + " | Números Negativos: " + neg + " | Ceros: " + ceros;
    document.getElementById("salidaP3").innerHTML = text;
}

// Problema 4

function promedios() {
	"use strict";
	var i, j, acum, prom, bodyHTML, tTable, tBody, tRow, tCell, ttCell, tHeader, mat = new Array(5), colProm = new Array(5);
	
    bodyHTML = document.getElementsByTagName("form")[3];
    tHeader = ['Columna 1', 'Columna 2', 'Columna 3', 'Columna 4', 'Columna 5', 'Promedio'];
    tTable = document.createElement("table");
    tBody = document.createElement("tbody");
	
	for (i = 0; i < 5; i++) {
		mat[i] = new Array(5);
		for (j = 0; j < 5; j++) {
			mat[i][j] = parseInt((Math.random() * 20) + 1, 10);
        }
    }
    
    for (i = -1; i < 5; i++) {
        tRow = document.createElement("tr");
        acum = 0;
        for (j = 0; j <= 5; j++) {
			tCell = document.createElement("td");
            if (i === -1) {
                ttCell = document.createTextNode(tHeader[j]);
            } else if (j !== 5) {
				acum += mat[i][j];
                ttCell = document.createTextNode(mat[i][j]);
            } else {
				prom = acum / 5;
				colProm[i] = prom.toFixed(2);
				ttCell = document.createTextNode(colProm[i]);
            }
            tCell.appendChild(ttCell);
            tRow.appendChild(tCell);
        }
        tBody.appendChild(tRow);
    }
    tTable.appendChild(tBody);
    bodyHTML.appendChild(tTable);
    tTable.setAttribute("border");
}

// Problema 5

function inverso() {
    "use strict";
	var i, num, text, digits, aux, num2 = [];

	num = document.getElementById("entrada5").value;
    digits = num.length;
    aux = digits;
    
    for (i = 0; i < digits; i++) {
		num2 = num2 + num[aux - 1];
        aux--;
    }
	text = "Número con sus dígitos invertidos: " + num2;
    document.getElementById("salidaP5").innerHTML = text;
}

// Problema 6

function calculate() {
	var i, width, height, mid;
	
	width = 1 / this.numRects;
	for (i = 0; i < this.numRects; i++) {
		mid = (i + 0.5) * width;
		height = 4.0 / (1.0 + mid * mid);
		this.sum += height;
	}
	this.area = width * this.sum;
	return this.area;
}

function CalculoPi(numRects, area, sum) {
	"use strict";
    this.numRects = numRects;
	this.area = area;
	this.sum = sum;
	this.calPi = calculate;
}

function problemaSeis() {
	"use strict";
	var num, cpi;
	
	num = document.getElementById("entrada6").value;
	num = parseInt(num, 10);
	cpi = new CalculoPi(num, 0, 0);
	document.getElementById("salidaP6").innerHTML = cpi.calPi();
}