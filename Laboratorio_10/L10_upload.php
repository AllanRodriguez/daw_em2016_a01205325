<?php
    session_start();

    if (isset($_POST["fileToUpload"]))
        $_SESSION["imageFile"] = "uploads/" . $_POST["fileToUpload"];

    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            // echo "File is an image - " . $check["mime"] . ".";            
            $uploadOk = 1;
        } else {
            // echo "File is not an image.";
            echo "<script type=\"text/javascript\">".
                "window.alert('File is not an image.');".
                'window.location.href="index.php";'."</script>";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        // echo "Sorry, file already exists.";
        echo "<script type=\"text/javascript\">".
                "window.alert('Sorry, file already exists.');".
                'window.location.href="index.php";'."</script>";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
        // echo "Sorry, your file is too large.";
        echo "<script type=\"text/javascript\">".
                "window.alert('Sorry, your file is too large.');".
                'window.location.href="index.php";'."</script>";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        // echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        echo "<script type=\"text/javascript\">".
                "window.alert('Sorry, only JPG, JPEG, PNG & GIF files are allowed.');".
                'window.location.href="index.php";'."</script>";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        // echo "Sorry, your file was not uploaded.";
        echo "<script type=\"text/javascript\">".
                "window.alert('Sorry, your file was not uploaded.');".
                'window.location.href="index.php";'."</script>";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            // echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
            echo "<script type=\"text/javascript\">".
                "window.alert('The file has been uploaded.');".
                'window.location.href="index.php";'."</script>";
        } else {
            // echo "Sorry, there was an error uploading your file.";
            echo "<script type=\"text/javascript\">".
                "window.alert('Sorry, there was an error uploading your file.');".
                'window.location.href="index.php";'."</script>";
        }
    }
?>