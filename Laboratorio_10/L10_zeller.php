<?php
    session_start();

    if (isset($_POST['day']) && isset($_POST['month']) && isset($_POST['year'])) {
        $d = $_POST['day'];
        $mes = ($_POST['month']);
        $m = obtainMonth($_POST['month']);
        $y = $_POST['year'];
        if (($m == 2) && ($d > 28)) {
            echo "<script type=\"text/javascript\">".
                "window.alert('¡FEBRERO solo tiene 28 días LOL!');".
                'window.location.href="index.php";'."</script>";
        }
        if (($m == 4) && ($d > 30)) {
            echo "<script type=\"text/javascript\">".
                "window.alert('¡ABRIL solo tiene 30 días LOL!');".
                'window.location.href="index.php";'."</script>";
        }
        if (($m == 6) && ($d > 30)) {
            echo "<script type=\"text/javascript\">".
                "window.alert('¡JUNIO solo tiene 30 días LOL!');".
                'window.location.href="index.php";'."</script>";
        }
        if (($m == 9) && ($d > 30)) {
            echo "<script type=\"text/javascript\">".
                "window.alert('¡SEPTIEMBRE solo tiene 30 días LOL!');".
                'window.location.href="index.php";'."</script>";
        }
        if (($m == 11) && ($d > 30)) {
            echo "<script type=\"text/javascript\">".
                "window.alert('¡NOVIEMBRE solo tiene 30 días LOL!');".
                'window.location.href="index.php";'."</script>";
        }
        $z = zeller($d, $m, $y);
        include("L10_zeller_view.html");
    } else {
        echo "<script type=\"text/javascript\">".
            "window.alert('¡Falta Llenar Alguno de los Campos!');".
            'window.location.href="index.php";'."</script>";
    }

    function zeller ($day, $month, $year) {
        $a = floor((14 - $month) / 12);
        $y = $year - $a;
        $m = $month + (12 * $a) - 2;
        $d = ($day + $y + floor($y/4) - floor($y/100) + floor($y/400) + floor((31*$m)/12)) % 7;
        return obtainDay($d);
    }

    function obtainMonth ($month) {
        switch ($month) {
            case "Enero":
                $m = 1;
                break;
            case "Febrero":
                $m = 2;
                break;
            case "Marzo":
                $m = 3;
                break;
            case "Abril":
                $m = 4;
                break;
            case "Mayo":
                $m = 5;
                break;
            case "Junio":
                $m = 6;
                break;
            case "Julio":
                $m = 7;
                break;
            case "Agosto":
                $m = 8;
                break;
            case "Septiembre":
                $m = 9;
                break;
            case "Octubre":
                $m = 10;
                break;
            case "Noviembre":
                $m = 11;
                break;
            case "Diciembre":
                $m = 12;
                break;
            default:
                $m = 1;
        }
        return $m;
    }

    function obtainDay ($day) {
        switch ($day) {
            case 0:
                $d = "Domingo";
                break;
            case 1:
                $d = "Lunes";
                break;
            case 2:
                $d = "Martes";
                break;
            case 3:
                $d = "Miércoles";
                break;
            case 4:
                $d = "Jueves";
                break;
            case 5:
                $d = "Viernes";
                break;
            case 6:
                $d = "Sábado";
                break;
            default:
                $d = "Domingo";
        }
        return $d;        
    }
?>