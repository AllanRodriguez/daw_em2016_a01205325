-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-03-2016 a las 08:11:44
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `productos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frutas`
--

CREATE TABLE `frutas` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(20) NOT NULL,
  `Tipo` varchar(20) NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Precio` int(11) NOT NULL,
  `Pais` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `frutas`
--

INSERT INTO `frutas` (`Id`, `Nombre`, `Tipo`, `Cantidad`, `Precio`, `Pais`) VALUES
(1, 'Platano', 'Dominico', 150, 15, 'Mexico'),
(2, 'Platano', 'Morado', 120, 20, 'Mexico'),
(3, 'Platano', 'Valery', 78, 18, 'Colombia'),
(4, 'Manzana', 'Golden_Delicious', 92, 25, 'EEUU'),
(5, 'Manzana', 'Royal_Gala', 34, 30, 'EEUU'),
(6, 'Manzana', 'Honey_Crisp', 205, 28, 'Espana'),
(7, 'Melocoton', 'Catherina', 50, 42, 'Espana'),
(8, 'Melocoton', 'Sudanell', 115, 38, 'Mexico'),
(9, 'Melocoton', 'Paraguayo', 87, 50, 'Paraguay'),
(10, 'Kiwi', 'Hayward', 112, 70, 'Nueva_Zelanda'),
(11, 'Kiwi', 'Summer', 205, 75, 'Brasil'),
(12, 'Kiwi', 'Top_Star', 78, 72, 'Italia');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `frutas`
--
ALTER TABLE `frutas`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `frutas`
--
ALTER TABLE `frutas`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
