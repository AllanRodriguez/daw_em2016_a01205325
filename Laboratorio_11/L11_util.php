<?php 
    function conectDb() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "productos";
        
        $myDb = mysqli_connect($servername, $username, $password, $dbname);
        
        // Check Connection
        if (!$myDb) {
            die("Connection failed: " . mysqli_connect_error());
        } else
            return $myDb;
    }
    
    // La variable $myDb es una conexión establecida anteriormente
    function closeDb($myDb) {
        mysqli_close($myDb);
    }

    function getFruits() {
        $myDb = conectDb();
        
        // Specification of the SQL query
        $query = 'SELECT * FROM `frutas`';
        // Query execution; returns identifier of the result group
        $result = mysqli_query($myDb, $query);
        #$result = $myDb->query($query);
        
        Display($result);
        
        // It releases the Associated Results
        mysqli_free_result($result);        
        closeDb($myDb);
    }

    // Consulta Uno
    function getFruitsByName($nombre) {
        $myDb = conectDb();
        
        // Specification of the SQL query        
        $query = 'SELECT * FROM `frutas` WHERE `Nombre` LIKE "'.$nombre.'"';
        // Query execution; returns identifier of the result group
        $result = mysqli_query($myDb, $query);
        #$result = $myDb->query($query);
        
        Display($result);
        
        // It releases the Associated Results
        mysqli_free_result($result);        
        closeDb($myDb);
    }

    // Consulta Dos
    function getFruitsByCountries($country1, $country2) {
        $myDb = conectDb();
        
        // Specification of the SQL query
        $query = 'SELECT * FROM `frutas` WHERE `Pais` LIKE "'.$country1.'" OR `Pais` LIKE "'.$country2.'"';
        // Query execution; returns identifier of the result group
        $result = mysqli_query($myDb, $query);
        #$result = $myDb->query($query);
        
        Display($result);
        
        // It releases the Associated Results
        mysqli_free_result($result);        
        closeDb($myDb);
    }

    // Consulta Tres
    function getFruitsByPrice($minPrice, $maxPrice) {
        $myDb = conectDb();
        
        // Specification of the SQL query
        $query = 'SELECT * FROM `frutas` WHERE `Precio` >= "'.$minPrice.'" AND `Precio` <= "'.$maxPrice.'"';
        // Query execution; returns identifier of the result group
        $result = mysqli_query($myDb, $query);
        #$result = $myDb->query($query);
        
        Display($result);
        
        // It releases the Associated Results
        mysqli_free_result($result);        
        closeDb($myDb);
    }

    function Display ($result) {
            echo "<table>";
            echo "<thead>";
            echo "<tr>";
            echo "<th>" . "Id" . "</th>";
            echo "<th>" . "Nombre" . "</th>";
            echo "<th>" . "Tipo" . "</th>";
            echo "<th>" . "Cantidad" . "</th>";
            echo "<th>" . "Precio" . "</th>";
            echo "<th>" . "Pais" . "</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
        
        // Cycle to Explode every Line of the Results
        while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
            // use of numeric index            
            echo "<tr>";
            echo "<td>" . $row['Id'] . "</td>";
            echo "<td>" . $row['Nombre'] . "</td>";
            echo "<td>" . $row['Tipo'] . "</td>";
            echo "<td>" . $row['Cantidad'] . "</td>";
            echo "<td>" . $row['Precio'] . "</td>";
            echo "<td>" . $row['Pais'] . "</td>";
            echo "</tr>";
        }
            echo "</tbody>";            
            echo "</table>";
    }
?>